package com.example.denis.countofstudents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG ="StartActivity" ;
    private Integer counter = 0;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
        Toast.makeText(this, "onStart()", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop");
        Toast.makeText(this, "onStop()", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counter",counter);
        Log.d(TAG,"onSaveInstanceState");
        Toast.makeText(this, "onSaveInstanceState", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState!=null && savedInstanceState.containsKey("counter")){
            counter=savedInstanceState.getInt("counter");
        }
        Log.d(TAG,"onRestoreInstanceState");
        Toast.makeText(this, "onRestoreInstanceState", Toast.LENGTH_LONG).show();
    }

    public void onClickBtnStudents(View view) {
        counter++;
        TextView counterViev = (TextView) findViewById(R.id.numberOfStudents);
        counterViev.setText(counter.toString());
        Log.d(TAG,"onClickBtnStudents");
        Toast.makeText(this, "onClickBtnStudents", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate");
        Toast.makeText(this, "onCreate", Toast.LENGTH_LONG).show();
    }



    @Override
    protected void onResume() {
        super.onResume();
        resetUI();
        Log.d(TAG, "onResume");
        Toast.makeText(this, "onResume", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        Toast.makeText(this, "onPause", Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
        Toast.makeText(this, "onRestart", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        Toast.makeText(this, "onDestroy", Toast.LENGTH_LONG).show();
    }



    private void resetUI() {
        ((TextView) findViewById(R.id.numberOfStudents)).setText(counter.toString());
        Log.d(TAG, "resetUI");
        Toast.makeText(this, "resetUI", Toast.LENGTH_LONG).show();
    }




   /** @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        Toast.makeText(this, "onCreateOptionsMenu", Toast.LENGTH_LONG).show();
        return true;
    }
*/

}
